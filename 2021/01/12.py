data = []

with open("input") as f:
    for line in f:
        data.append(int(line.strip()))

count = 0
vpriv = 0

for i, d in enumerate(data):
    if i > 1:
        l = []
        l.extend([data[i - 2], data[i - 1], data[i]])
        v = sum(l)
        print(v, vpriv)
        if v > vpriv and vpriv != 0:
            count += 1
        vpriv = v

print(count)
