data = []

with open("input") as f:
    for line in f:
        data.append(int(line.strip()))

count = 0

for i, d in enumerate(data):
    if i > 0 and data[i] > data[i - 1]:
        count += 1

print(count)
