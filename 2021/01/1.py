def one(indata):
    count = 0

    for i, d in enumerate(data):
        if i > 0 and data[i] > data[i - 1]:
            count += 1

    return count


def two(indata):
    count = 0

    for i, d in enumerate(data):
        if i > 2:
            if (
                data[i - 2] + data[i - 1] + data[i]
                > data[i - 3] + data[i - 2] + data[i - 1]
            ):
                count += 1
    return count


def main(data):

    # print("indata", data)
    print("One", one(data))
    print("Two", two(data))


if __name__ == "__main__":
    # indata = "test"
    indata = "input"
    data = []

    with open(indata) as f:
        for line in f:
            data.append(int(line.strip()))

    main(data)
