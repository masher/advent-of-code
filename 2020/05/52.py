import fileinput

data = [line.rstrip() for line in fileinput.input()]

seats = []
nrows = 128
ncollums = 8


def half(li, letter):

    half = int(len(li) / 2)
    if letter == "B" or letter == "R":
        return li[half:]
    elif letter == "F" or letter == "L":
        return li[:half]


for d in data:
    rows = [i for i in range(nrows)]
    collums = [i for i in range(ncollums)]

    for index, i in enumerate(d):

        if index < 7:
            rows = half(rows, i)
        if index > 6:
            collums = half(collums, i)

    seats.append(int(rows[0]) * 8 + int(collums[0]))

print(max(seats))

difflist = list(range(min(seats), max(seats)))
print(set(seats) ^ set(difflist))
