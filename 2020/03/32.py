import fileinput

data = list(fileinput.input())
end = len(data)

treeslist = []

slope = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]


def moveright(pos, slope):
    newpos = pos + slope
    return newpos - 31 if newpos >= 31 else newpos


sumtrees = 1
for s in slope:

    x = 0
    y = 0
    trees = 0
    y = 0
    while y < end:
        x = moveright(x, s[0])
        y += s[1]
        if y < end and data[y][x] == "#":
            trees += 1

    sumtrees *= trees
    treeslist.append(trees)

print(treeslist)
print(sumtrees)
