import fileinput
import os

data = list(fileinput.input())
end = len(data)

x = 0
y = 0
trees = 0


def moveright(pos):
    newpos = pos + 3
    return newpos - 31 if newpos >= 31 else newpos


for r in range(end):
    x = moveright(x)
    y = r + 1
    try:
        if data[y][x] == "#":
            trees += 1
    except:
        pass

print(trees)
