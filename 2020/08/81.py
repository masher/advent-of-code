import fileinput

data = [f.strip() for f in fileinput.input()]

acc = 0
i = 0
seen = set()
while i < len(data) + 1:
    print(i, data[i])
    if i in seen:
        print("seen")
        break
    else:
        seen.add(i)

    k, v = data[i].split(" ")

    if k == "nop":
        i += 1
    elif k == "acc":
        acc = acc + int(v)
        i += 1
    elif k == "jmp":
        i = i + int(v)
    if i == len(data):
        i = 0

print(acc)
