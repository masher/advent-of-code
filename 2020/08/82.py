import fileinput

data = [f.strip() for f in fileinput.input()]

acc = 0
i = 0

change = []


def checkfunction(indata, inline, tmpv):

    acc = 0
    i = 0

    seen = set()
    while i < len(indata) + 1:

        if i in seen:
            break
        else:
            seen.add(i)

        k, v = indata[i].split(" ")

        if i == int(inline):
            k = tmpv

        if k == "nop":
            i += 1
        elif k == "acc":
            acc = acc + int(v)
            i += 1
        elif k == "jmp":
            i = i + int(v)

        if i == len(indata):
            print(acc)
            break


for d in data:
    if d.split(" ")[0] == "nop" or d.split(" ")[0] == "jmp":
        change.append(str(i) + d.split(" ")[0])

    i += 1


for c in change:
    row = c[:-3]
    if c[-3:] == "nop":
        tmpv = "jmp"
    else:
        tmpv = "nop"

    checkfunction(data, row, tmpv)
