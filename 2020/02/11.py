import fileinput
import math
import os
import re
import sys
from collections import Counter

x = [line.rstrip() for line in fileinput.input()]
n = len(x)


def counting(letter, minletter, maxletter, data):
    c = Counter(data)
    print(c)


y = 0

for i in range(n):
    tmp, two, data = x[i].split(" ")
    arg1, arg2 = tmp.split("-")

    if two[0] in set(data):
        tmpdata = re.findall(two[0], data)
        if tmpdata.count(two[0]) >= int(arg1):
            if tmpdata.count(two[0]) <= int(arg2):
                y += 1

print(y)
