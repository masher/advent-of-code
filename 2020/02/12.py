import fileinput
import math
import os
import re
import sys
from collections import Counter

x = list(fileinput.input())

y = 0
for i in x:
    lines = i.strip().split()
    arg1, arg2 = [int(c) for c in lines[0].split("-")]

    ch = lines[1][0]
    data = lines[2]

    if (data[arg1 - 1] == ch) ^ (data[arg2 - 1] == ch):
        print(ch, data, data[arg1 - 1], data[arg2 - 1])
        y += 1

print(y)
