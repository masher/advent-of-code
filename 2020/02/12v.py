import fileinput
import math
import os
import re
import sys
from collections import Counter

x = [line.rstrip() for line in fileinput.input()]
n = len(x)
y = 0

for i in range(n):
    tmp, two, data = x[i].split(" ")
    arg1, arg2 = tmp.split("-")

    if two[0] in set(data):
        try:
            stringpos = [pos + 1 for pos, char in enumerate(data) if char == two[0]]
            # print(arg1, arg2, two[0], data, stringpos)
            if int(arg1) in stringpos or int(arg2) in stringpos:
                if int(arg1) in stringpos and int(arg2) in stringpos:
                    # print(arg1, arg2, stringpos)
                    pass
                else:
                    y += 1
        except:
            pass

print(y)
