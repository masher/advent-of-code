import re
from collections import defaultdict

with open("indata.in", "r") as f:
    indata = f.read().split("\n")

# print(indata)

value = 0
tmp = set()

for index, i in enumerate(indata):
    if len(i) == 0:
        tmp.clear()
    else:
        for k in i:
            if k not in tmp:
                value += 1
                tmp.add(k)
print(value)
