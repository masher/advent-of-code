import string

with open("indata.in", "r") as f:
    indata = f.read().split("\n")


value = 0
alpa = string.ascii_lowercase
tmp = set(alpa)

for index, i in enumerate(indata):
    if len(i) == 0:
        value += len(tmp)
        tmp.clear()
        tmp = set(alpa)
    else:
        tmp2 = set([k for k in i])
        tmp = tmp.intersection(tmp2)
print(value)
