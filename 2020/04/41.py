import re
from collections import defaultdict

with open("indata.in", "r") as f:
    indata = f.read().split("\n")


needed = ["iyr", "ecl", "byr", "hcl", "eyr", "hgt", "pid"]
hair = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
tmp = []
passports = []

ok = 0


def checked(input):
    tmp.append(input)


def pushtmp(input):
    for i in input.split(" "):
        k, v = i.split(":")
        if k not in tmp:
            # print(k, v)
            if k == "byr" and (1920 <= int(v) <= 2002):
                checked(k)
            elif k == "iyr" and (2010 <= int(v) <= 2020):
                checked(k)
            elif k == "eyr" and (2020 <= int(v) <= 2030):
                checked(k)
            elif k == "hgt":
                if v[-1] == "m" and (150 <= int(v[:-2]) <= 193):
                    checked(k)
                elif v[-1] == "n" and (59 <= int(v[0:-2]) <= 76):
                    checked(k)
            elif (
                k == "hcl"
                and v[0] == "#"
                and re.findall(r"[a-f0-9]", v[1:])
                and len(v[1:]) == 6
            ):
                checked(k)
            elif k == "ecl" and v in hair:
                checked(k)

            elif k == "pid" and len(v) == 9 and re.findall(r"[0-9]", v):
                checked(k)


def chunkend():
    passports.append(tmp)


for index, i in enumerate(indata):

    if index + 1 == len(indata):
        pushtmp(i)
        chunkend()
    elif len(i) > 0:
        pushtmp(i)
    elif len(i) == 0:
        chunkend()
        tmp = []

for p in passports:
    print(p)
    if all(elem in p for elem in needed):
        ok += 1

print(ok)
