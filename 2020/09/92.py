import fileinput

data = list(int(i.lstrip()) for i in fileinput.input())
v = 25
container = []

for d in range(v, len(data)):
    """Start at 25, create all num pairs and sum. Compare if not in set."""
    tmpdata = [(x, y) for x in data[d - v : d] for y in data[d - v : d]]
    numbers = set([sum(x) for x in tmpdata])
    if data[d] not in numbers:
        rightnumber = data[d]


print(rightnumber)

i = 0
index = 0

while i < len(data):
    """Start from index0, add values until right or too big. Move index."""
    container.append(data[i])

    if sum(container) == rightnumber:
        smallest = min(container)
        biggest = max(container)
        total = smallest + biggest
        print("Win! > ", smallest, biggest, total)

        container.clear()
        index += 1
        i = index
    elif sum(container) > rightnumber:
        container.clear()
        index += 1
        i = index
    else:
        i += 1
