import fileinput

data = list(int(i.lstrip()) for i in fileinput.input())
v = 25

for d in range(v, len(data)):

    tmpdata = [(x, y) for x in data[d - v : d] for y in data[d - v : d]]
    numbers = set([sum(x) for x in tmpdata])
    if data[d] not in numbers:
        print(data[d])
