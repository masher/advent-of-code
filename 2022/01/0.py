from collections import defaultdict

# import pandas as pd


def one(indata):

    sum_data = []
    x = 0

    for i in indata:
        if len(i) > 0:
            x = x + int(i)
        else:
            sum_data.append(x)
            x = 0
    return sum_data


def two(indata):

    pass


def main(data):

    # print("indata", data)
    one_data = one(data)
    # print(one_data)
    print("One", max(one_data))
    print("Two", sum(sorted(one_data)[-3:]))


if __name__ == "__main__":
    # indata = "input_test"
    indata = "input"
    data = []

    with open(indata) as f:
        for line in f:
            data.append(line.strip())

    # with open(indata) as f:
    # lines = f.read()
    # lines = f.readline()
    # lines = f.readlines()

    main(data)
