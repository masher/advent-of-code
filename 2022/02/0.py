# import pandas as pd


def one(indata):

    points = []

    for i in indata:
        if i == "AX":
            points.append(4)
        elif i == "BX":
            points.append(1)
        elif i == "CX":
            points.append(7)
        elif i == "AY":
            points.append(8)
        elif i == "BY":
            points.append(5)
        elif i == "CY":
            points.append(2)
        elif i == "AZ":
            points.append(3)
        elif i == "BZ":
            points.append(9)
        elif i == "CZ":
            points.append(6)

    return sum(points)


def two(indata):

    points = []

    for i in indata:
        if i == "AX":
            points.append(3)
        elif i == "BX":
            points.append(1)
        elif i == "CX":
            points.append(2)
        elif i == "AY":
            points.append(4)
        elif i == "BY":
            points.append(5)
        elif i == "CY":
            points.append(6)
        elif i == "AZ":
            points.append(8)
        elif i == "BZ":
            points.append(9)
        elif i == "CZ":
            points.append(7)

    return sum(points)


def main(data):

    # print("indata", data)
    print("One", one(data))
    print("Two", two(data))


if __name__ == "__main__":
    # indata = "input_test"
    indata = "input"
    data = []

    with open(indata) as f:
        for line in f:
            data.append(line.strip().replace(" ", ""))

    # with open(indata) as f:
    # lines = f.read()
    # lines = f.readline()
    # lines = f.readlines()

    # print(lines)

    main(data)
