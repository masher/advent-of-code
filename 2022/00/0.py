from collections import defaultdict

import pandas as pd


def one(indata):

    pass


def two(indata):

    pass


def main(data):

    # print("indata", data)
    print("One", one(data))
    print("Two", two(data))


if __name__ == "__main__":
    # indata = "test"
    indata = "input"
    data = []

    with open(indata) as f:
        for line in f:
            data.append(int(line.strip()))

    with open("indata") as f:
        lines = f.read()
        lines = f.readline()
        lines = f.readlines()

    main(data)
