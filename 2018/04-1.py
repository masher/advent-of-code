from collections import defaultdict
from collections import Counter
from statistics import mode

with open('04.csv') as f:
    indata = f.read()

numbers = indata.split('\n'
                       )

time = []
guards = defaultdict(list)

for n in numbers:
    s = n[1:17]
    time.append(n)

time.sort()

# '[1518-03-15 00:01] Guard #709 begins shift'

tmpguard = ''
timestart = ''
timesend = ''

for x in time:

    if x.split(' ')[2].split(' ')[0] == 'Guard':
        tmpguard = x.split('#')[1].split(' ')[0]
    elif x.split(' ')[2].split(' ')[0] == 'falls':
        timestart = x.split(':')[1].split(']')[0]
    elif x.split(' ')[2].split(' ')[0] == 'wakes':
        timesend = x.split(':')[1].split(']')[0]
        for t in range(int(timestart), int(timesend)):
            guards[tmpguard].append(t)
            t += 1


maxguard = max(guards, key=lambda x: len(guards[x]))
mostmin = mode(guards[maxguard])

print(int(mostmin)*int(maxguard))
