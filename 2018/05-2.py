import re
from collections import Counter

with open('/home/hampus/dev/python/advent-of-code/2018/05.csv') as f:
    indata = f.read()


loopset = (set(indata.lower()))
sim = []
print(loopset)
for x in loopset:
    tmpindata = indata
    tmpindata = re.sub(x, '', tmpindata)
    tmpindata = re.sub(x.upper(), '', tmpindata)

    i = 0
    while i < len(tmpindata)-1:
        if tmpindata[i+1] == tmpindata[i].swapcase():
            tmp = (tmpindata[i]+tmpindata[i+1])
            print(tmp)
            tmpindata = re.sub(tmp, '', tmpindata)
            i = 0
            print('restart')
        else:
            i += 1

    sim.append(len(tmpindata))

print(sim)
print(min(sim))
