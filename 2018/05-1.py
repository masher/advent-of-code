import re
import sys

with open('/home/hampus/dev/python/advent-of-code/2018/05.csv') as f:
    indata = f.read()


i = 0
while i < len(indata)-1:
    if indata[i+1] == indata[i].swapcase():
        tmp = (indata[i]+indata[i+1])
        print(tmp)
        indata = re.sub(tmp, '', indata)
        i = 0
        print('restart')
    else:
        i += 1

print(len(indata))
# # print(len(set(dogfood)))
