from itertools import permutations
import collections
# 1365 @ 824,458: 28x17
# 1 @ 850,301: 23x12

with open('03.csv') as f:
    indata = f.read()

numbers = indata.split('\n'
                       )

cloth = []
idlist = []

for x in numbers:

    tmpl = []
    tmpw = []
    tmpid = []
    tmpid2 = []

    idnr = y = x.split('@')[0]
    y = x.split('@')[1]
    left = int(y.split(',')[0].strip())+1
    t0 = y.split(',')[1]
    top = int(t0.split(':')[0])+1
    w = int(t0.split(':')[1].split('x')[0].strip())
    h = int(t0.split(':')[1].split('x')[1].strip())
    # print(y)
    # print(left)
    # print(top)
    # print(t0)
    # print(w)
    # print(h)
    # print(type(l))

    tmpid.append(idnr)

    for i in range(left, int(left)+w):
        tmpl.append('L'+str(i))

    for i in range(top, int(top)+h):
        tmpw.append('w'+str(i))

    # tmp = ([list(zip(tmpl, p)) for p in permutations(tmpw)])
    tmp = [[x, y] for x in tmpl for y in tmpw]

    for m in tmp:
        cloth.append(''.join(m))

    for m in tmp:
        tmpid2.append(''.join(m))

    tmpid.append(tmpid2)
    idlist.append(tmpid)

c = collections.Counter(cloth)

myList = [k for k, v in c.items() if v == 1]

for x in idlist:
    if set(x[1]).issubset(myList):
        print(x[0])
