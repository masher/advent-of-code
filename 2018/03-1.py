from itertools import permutations
import collections
# 1365 @ 824,458: 28x17
# 1 @ 850,301: 23x12

with open('03.csv') as f:
    indata = f.read()

numbers = indata.split('\n'
                       )

cloth = []
multi = 0

for x in numbers:

    tmpl = []
    tmpw = []

    y = x.split('@')[1]
    left = int(y.split(',')[0].strip())+1
    t0 = y.split(',')[1]
    top = int(t0.split(':')[0])+1
    w = int(t0.split(':')[1].split('x')[0].strip())
    h = int(t0.split(':')[1].split('x')[1].strip())
    # print(y)
    # print(left)
    # print(top)
    # print(t0)
    # print(w)
    # print(h)
    # print(type(l))

    for i in range(left, int(left)+w):
        # print('L'+str(i))
        tmpl.append('L'+str(i))

    for i in range(top, int(top)+h):
        # print('w'+str(i))
        tmpw.append('w'+str(i))

    # print([list(zip(tmpl, p)) for p in permutations(tmpw)])
    tmp = [[x, y] for x in tmpl for y in tmpw]
    # print(tmp)

    for m in tmp:
        cloth.append(''.join(m))
# print(cloth)
c = collections.Counter(cloth)

myList = [k for k, v in c.items() if v > 1]

print(len(myList))
