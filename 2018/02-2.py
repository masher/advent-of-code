
with open('02.csv') as f:
    indata = f.read()

numbers = indata.split('\n'
                       )


def check(indata):
    for x in numbers:
        word = []
        for y, z in zip(indata, x):
            if y == z:
                word.append(y)
            else:
                pass
        if len(word) == len(indata)-1:
            print(''.join(word))


for n in numbers:
    check(n)
