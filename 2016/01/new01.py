directions = [0, 0, 0, 0]
direction = 0

with open('input1.csv') as f:
    indata = f.read()

tokens = indata.split(', ')

for token in tokens:
    letter = token[0]
    steps = int(token[1:])

    if letter == 'R':
        direction -= 1
    else:
        direction += 1
    direction %= len(directions)
    print(letter)
    print(steps)
    print(direction)
    print(directions)
    directions[direction] += steps

print(abs(directions[0]-directions[2])+abs(directions[1]-directions[3]))
