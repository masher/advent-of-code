

# with open('02.csv') as f:
#     indata = f.read()

# packets = indata.split('\n'
#                        )


# # def paper(l, w, h):
# #     print((2*l*w)+(2*w*h)+(2*h*l))
# #     return((2*l*w)+(2*w*h)+(2*h*l))

# def ribbon(small, nextsmall):
#     print(small)
#     print(nextsmall)
#     return(small+small+nextsmall+nextsmall)


# meter = 0

# for packet in packets:
#     packetdata = (packet.split('x'))
#     l = int(packetdata[0])
#     w = int(packetdata[1])
#     h = int(packetdata[2])
#     # small = int(min(packetdata))
#     nextsmallest = int(sorted(packetdata)[1])
#     small = int(sorted(packetdata)[0])

#     # print(small)
#     # print(nextsmallest)
#     meter += (ribbon(small, nextsmallest))+(l*w*h)
#     # +(small*nextsmallest)

# print(meter)

with open('/home/hampus/dev/python/advent-of-code/2015/02.csv') as f:
    indata = f.read()

packets = indata.split('\n'
                       )


def ribbon(l, w, h):
    long = max(max(l, w), h)
    perim = 2*(+w+h)-2*long
    return(perim)


meter = 0

for packet in packets:
    packetdata = (packet.split('x'))
    l = int(packetdata[0])
    w = int(packetdata[1])
    h = int(packetdata[2])
    small = min(l*w, w*h)
    # small = int(min(packetdata))
    # nextsmallest = int(sorted(packetdata)[1])
    # small = int(sorted(packetdata)[0])
    # print(small)
    # print(nextsmallest)
    meter += (ribbon(l, w, h))+(l*w*h)
    # +(small*nextsmallest)

print(meter)
