export1 = []


def get_data(file):
    with open(file) as f:
        indata = [line.rstrip() for line in f]
    return indata


def part_one():
    data = get_data('3.csv')
    index = 0

    totalCords = []

    for lines in data:

        x = 0
        y = 0

        emptyOne = []

        for g in [x for x in data[index].split(',')]:

            if g[0] == 'R':
                for i in range(0, int(g[1:])):
                    x += 1
                    emptyOne.append([x, y])
            elif g[0] == 'L':
                for i in range(0, int(g[1:])):
                    x -= 1
                    emptyOne.append([x, y])
            elif g[0] == 'U':
                for i in range(0, int(g[1:])):
                    y += 1
                    emptyOne.append([x, y])
            elif g[0] == 'D':
                for i in range(0, int(g[1:])):
                    y -= 1
                    emptyOne.append([x, y])

        totalCords.append(emptyOne)
        index += 1

    s1 = set(tuple(row) for row in totalCords[0])
    s2 = set(tuple(row) for row in totalCords[1])
    s3 = s1.intersection(s2)
    nums = []

    for s in s3:
        nums.append(abs(s[0])+abs(s[1]))
        export1.append(s)

    return(min(nums))


def part_two(lookfor):

    with open('3.csv') as f:
        data = f.read().splitlines()

    steps = []

    for l in lookfor:
        index2 = 0

        for lines in data:

            x = 0
            y = 0
            count = 0

            for g in data[index2].split(','):

                if g[0] == 'R':
                    for i in range(0, int(g[1:])):
                        x += 1
                        count += 1
                        if x == l[0] and y == l[1]:
                            steps.append([x, y, count])
                elif g[0] == 'L':
                    for i in range(0, int(g[1:])):
                        x -= 1
                        count += 1
                        if x == l[0] and y == l[1]:
                            steps.append([x, y, count])
                elif g[0] == 'U':
                    for i in range(0, int(g[1:])):
                        y += 1
                        count += 1
                        if x == l[0] and y == l[1]:
                            steps.append([x, y, count])
                elif g[0] == 'D':
                    for i in range(0, int(g[1:])):
                        y -= 1
                        count += 1
                        if x == l[0] and y == l[1]:
                            steps.append([x, y, count])

            index2 += 1

    steps2 = []
    index3 = -1
    for s in steps:
        if s[0] == steps[index3][0] and s[1] == steps[index3][1]:
            steps2.append(s[2] + steps[index3][2])

        index3 += 1

    return(min(steps2))


print(part_one())
print(part_two(export1))
