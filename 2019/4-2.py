from collections import Counter

indata = '165432-707912'

passwords = []


def verify(input):
    a = list(map(int, str(input)))
    b = a[:3]
    c = a[3:]

    if b[2] <= c[0]:
        if b[0] <= b[1] <= b[2] and c[0] <= c[1] <= c[2]:
            return(input)


def double(input):

    nums = ['00', '11', '22', '33', '44', '55', '66', '77', '88', '99']
    return(any(num in str(input) for num in nums))


def one(start, end):

    for x in range(start, end):
        if verify(x):
            if double(x):
                passwords.append(x)
    return(len(passwords))


def two():

    passwords2 = []
    index = 0

    for p in range(len(passwords)):
        if any(v == 2 for v in Counter(str(passwords[index])).values()):
            passwords2.append(passwords[index])
        index += 1

    return(len(passwords2))


print(one(165432, 707912))
print(two())
