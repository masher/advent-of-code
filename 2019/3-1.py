def get_data(file):
    with open(file) as f:
        indata = [line.rstrip() for line in f]
    return indata


def part_one():
    data = get_data('3.csv')

    groupOne = [x for x in data[0].split(',')]
    groupTwo = [x for x in data[1].split(',')]

    x = 0
    y = 0
    emptyOne = []

    for g in groupOne:
        if g[0] == 'R':
            for i in range(0, int(g[1:])):
                x += 1
                emptyOne.append([x, y])
        elif g[0] == 'L':
            for i in range(0, int(g[1:])):
                x -= 1
                emptyOne.append([x, y])
        elif g[0] == 'U':
            for i in range(0, int(g[1:])):
                y += 1
                emptyOne.append([x, y])
        elif g[0] == 'D':
            for i in range(0, int(g[1:])):
                y -= 1
                emptyOne.append([x, y])
    # print(emptyOne)

    s1 = set(tuple(row) for row in emptyOne)
    # print(s1)

    x = 0
    y = 0
    emptyTwo = []

    for g in groupTwo:
        if g[0] == 'R':
            for i in range(0, int(g[1:])):
                x += 1
                emptyTwo.append([x, y])
        elif g[0] == 'L':
            for i in range(0, int(g[1:])):
                x -= 1
                emptyTwo.append([x, y])
        elif g[0] == 'U':
            for i in range(0, int(g[1:])):
                y += 1
                emptyTwo.append([x, y])
        elif g[0] == 'D':
            for i in range(0, int(g[1:])):
                y -= 1
                emptyTwo.append([x, y])
    # print(emptyOne)

    s2 = set(tuple(row) for row in emptyTwo)
    # print(s1)
    s3 = s1.intersection(s2)
    print(s3)

    nums = []

    for s in s3:
        nums.append(abs(s[0])+abs(s[1]))

    return(min(nums))


print(part_one())
