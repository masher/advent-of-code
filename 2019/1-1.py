import math
import csv

summa = []


def main():

    with open('1-1.csv') as f:
        indata = f.read().splitlines()

    # print(indata)
    int_lst = [int(x) for x in indata]

    for i in int_lst:
        summa.append(math.floor(i/3)-2)

    print(sum(summa))


if __name__ == "__main__":
    main()
