
def get_data(file):
    with open(file) as f:
        indata = [l.strip().split(')') for l in f]
    return indata


def get_orbs(child, orbits):
    c = orbits.get(child)

    if not c:
        return []

    return [c] + get_orbs(c, orbits)


def one():
    data = get_data('6.csv')
    number = []

    orbits = {D: c for c, D in data}
    print(orbits)

    for obj in orbits.keys():
        # print(obj)
        number.append(get_orbs(obj, orbits))

    count = sum(len(x) for x in number)
    return(count)


def two():
    data = get_data('6.csv')

    orbits = {D: c for c, D in data}

    you = get_orbs('YOU', orbits)
    santa = get_orbs('SAN', orbits)

    # b = [x for x in you if not x in santa]

    steps = len(set(you) ^ set(santa))
    return(steps)


print(one())
print(two())
