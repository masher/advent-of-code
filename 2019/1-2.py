import math
import csv

summa = []


def calulation(i):
    return(math.floor(i/3)-2)


def mass(input):
    delsumma = []
    v = input
    while v > 0 and not None:
        print(v)
        v = calulation(v)
        delsumma.append(v)

    return(sum(delsumma)+abs(v))


def main():

    with open('1-1.csv') as f:
        indata = [int(x) for x in f.read().splitlines()]

    # indata = [1969]

    for i in indata:
        s = mass(i)
        summa.append(s)

    print(sum(summa))


if __name__ == "__main__":
    main()
