indata = '165432-707912'

passwords = []


def verify(input):
    a = list(map(int, str(input)))
    b = a[:3]
    c = a[3:]

    if b[2] <= c[0]:
        if b[0] <= b[1] <= b[2] and c[0] <= c[1] <= c[2]:
            return(input)


def double(input):

    nums = ['00', '11', '22', '33', '44', '55', '66', '77', '88', '99']
    return(any(num in str(input) for num in nums))


for x in range(165432, 707912):
    if verify(x):
        if double(x):
            passwords.append(x)


print(len(passwords))
