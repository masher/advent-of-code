import math
import csv


def main():

    summa = 0
    with open('1-1.csv') as f:
        indata = [int(x) for x in f.read().splitlines()]

    # indata = [1969]

    for i in indata:
        while i > 8:
            i = math.floor(i/3) - 2
            summa += i

    print(summa)


if __name__ == "__main__":
    main()
