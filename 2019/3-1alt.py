def get_data(file):
    with open(file) as f:
        indata = [line.rstrip() for line in f]
    return indata


def part_one():
    data = get_data('3.csv')
    index = 0

    totalCords = []

    for lines in data:

        # group = [x for x in data[index].split(',')]

        x = 0
        y = 0

        emptyOne = []

        for g in [x for x in data[index].split(',')]:
            if g[0] == 'R':
                for i in range(0, int(g[1:])):
                    x += 1
                    emptyOne.append([x, y])
            elif g[0] == 'L':
                for i in range(0, int(g[1:])):
                    x -= 1
                    emptyOne.append([x, y])
            elif g[0] == 'U':
                for i in range(0, int(g[1:])):
                    y += 1
                    emptyOne.append([x, y])
            elif g[0] == 'D':
                for i in range(0, int(g[1:])):
                    y -= 1
                    emptyOne.append([x, y])

        totalCords.append(emptyOne)
        index += 1

    s1 = set(tuple(row) for row in totalCords[0])
    s2 = set(tuple(row) for row in totalCords[1])
    s3 = s1.intersection(s2)
    nums = []

    for s in s3:
        nums.append(abs(s[0])+abs(s[1]))

    return(min(nums))


print(part_one())
