def get_data(file):
    with open(file) as f:
        indata = [int(x) for x in f.read().split(',')]
    return indata


def part_one(input):
    data = get_data('5.csv')

    x = 0
    output = []

    while x < len(data):

        op = data[x] % 100
        # op = str(data[x]).zfill(5)
        mod1 = (data[x] // 100) % 10
        mod2 = (data[x] // 1000) % 10
        mod3 = (data[x] // 10000) % 10

        if op == 99:
            break

        p1 = x + 1 if mod1 == 1 else data[x + 1]
        p2 = x + 2 if mod2 == 1 else data[x + 2]
        p3 = x + 3 if mod3 == 1 else data[x + 3]

        if op == 1:
            data[p3] = data[p1] + data[p2]
            x += 4
        elif op == 2:
            data[p3] = data[p1] * data[p2]
            x += 4
        elif op == 3:
            data[p1] = input
            x += 2
        elif op == 4:
            output.append(data[p1])
            x += 2
        elif op == 5:
            pass
        elif op == 6:
            pass
        elif op == 7:
            pass
        elif op == 8:
            pass

        # if op == 1:
        #     y = data[x+1] if mod1 == 0 else x+1
        #     v = data[x+2] if mod2 == 0 else x+2
        #     p = data[x+3] if mod3 == 0 else x+3
        #     data[p
        # ] = data[y]+data[v]
        #     x += 4
        # elif op == 2:
        #     y = data[x+1] if mod1 == 0 else x+1
        #     v = data[x+2] if mod2 == 0 else x+2
        #     p = data[x+3] if mod3 == 0 else x+3
        #     data[p] = data[y]*data[v]
        #     x += 4
        # elif op == 3:  # input
        #     data[x+1] = input
        #     x += 2
        # elif op == 4:  # output
        #     y = data[x+1]
        #     output.append(y)
        #     x += 2

    print(output)
    return data[0]


# def part_two():
#     for x in range(100):
#         for y in range(100):
#             if part_one(x, y) == 19690720:
#                 return 100*x+y

part_one(1)
# print(part_two())
