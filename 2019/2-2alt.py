def get_data(file):
    with open(file) as f:
        indata = [int(x) for x in f.read().split(',')]
    return indata


def part_one(value1, value2):
    data = get_data('2-1.csv')
    data[1] = value1
    data[2] = value2
    # x = 0

    for x in range(0, len(data), 4):
        if data[x] == 1:
            y = data[x+1]
            v = data[x+2]
            p = data[x+3]
            data[p] = data[v]+data[y]
        elif data[x] == 2:
            y = data[x+1]
            v = data[x+2]
            p = data[x+3]
            data[p] = data[v]*data[y]
        elif data[x] == 99:
            break
    return data[0]


def part_two():
    for x in range(100):
        for y in range(100):
            if part_one(x, y) == 19690720:
                return 100*x+y


print(part_one(12, 2))
print(part_two())
