def main():

    with open('2-1.csv') as f:
        indata = [int(x) for x in f.read().split(',')]

    pos = 0
    x = 0

    # indata = [1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50]

    indata[1] = 12
    indata[2] = 2

    while x < len(indata):
        if indata[x] == 1:
            y = indata[x+1]
            v = indata[x+2]
            p = indata[x+3]
            indata[p] = indata[v]+indata[y]
        elif indata[x] == 2:
            y = indata[x+1]
            v = indata[x+2]
            p = indata[x+3]
            indata[p] = indata[v]*indata[y]
        elif indata[x] == 99:
            print(indata)
            break

        x += 4

    # print(indata)


if __name__ == "__main__":

    main()
